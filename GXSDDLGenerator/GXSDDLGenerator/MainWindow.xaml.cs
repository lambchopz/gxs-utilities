﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.ComponentModel;
using System.Threading;
using System.Text.RegularExpressions;

namespace GXSDDLGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int seedboxValue = 0;
        private int optionsValue = 0; //0 = Links Only, 1 = Non-Batch Posts, 2 = Batch Posts
        private double totalFilesize = 0;
        private int postingGenStatus = 0; //0 = state for generating DDL, 1 = state for generating Torrent
        private string overwriteText = "";
        private string batchTorrentFile = "";
        private string batchTorrentLink = "";
        private BackgroundWorker generator = new BackgroundWorker();
        

        public MainWindow()
        {
            InitializeComponent();
            generator.WorkerReportsProgress = true;
            generator.WorkerSupportsCancellation = true;
            generator.DoWork += new DoWorkEventHandler(Generator_DoWork);
            generator.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Generator_RunWorkerCompleted);
            generator.ProgressChanged += Generator_ProgressChanged;
        }


        private void SeedboxChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox seedboxCombobox = sender as ComboBox;
            seedboxValue = seedboxCombobox.SelectedIndex;
        }

        private void OptionsChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox optionsCombobox = sender as ComboBox;
            optionsValue = optionsCombobox.SelectedIndex;
        }

        public void OnDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }

        private void OnDrop(object sender, DragEventArgs e)
        {
            Console.WriteLine("Files Dropped");
            totalFilesize = 0;
            postingGenStatus = 0;
            batchTorrentFile = "";
            batchTorrentLink = "";
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            try
            {
                foreach (string file in files)
                {
                    break;
                }
            }
            catch
            {
                StatusLabel.Text = "Some directories or files provided failed to load. Check if the file is opened or still exists.";
                return;
            }

            StatusLabel.Text = "Generating ddl links...";
            SeedboxComboBox.IsEnabled = false;
            GeneratorTextBox.Text = "";
            overwriteText = OverwriteTextBox.Text;
            if (!generator.IsBusy)
            {
                generator.RunWorkerAsync(files);
            }
            else
            {
                generator.CancelAsync();
                generator.RunWorkerAsync(files);
            }
        }

        private void Generator_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] files = (string[])e.Argument;

            try {
                foreach (string file in files)
                {
                    if (Directory.Exists(file))
                    {
                        ProcessDirectory(file);
                    }
                    else if (File.Exists(file))
                    {
                        ProcessFile(file);
                    }
                }
                if (optionsValue == 2 && !batchTorrentLink.Equals(""))
                {
                    string filename = System.IO.Path.GetFileName(batchTorrentFile);
                    string htmltext = "";

                    Console.WriteLine("Printing Torrent Batch Link");

                    postingGenStatus = 1;
                    htmltext = ParseFilename(filename, batchTorrentFile, batchTorrentLink);
                    generator.ReportProgress(0, htmltext + "\n");
                }
            }
            catch
            {
                e.Cancel = true;
                generator.CancelAsync();
            }
        }

        private void Generator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            GeneratorTextBox.AppendText(e.UserState as string);
        }

        private void Generator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                StatusLabel.Text = "An invalid file or format was provided";
            }
            else
            {
                StatusLabel.Text = "DDL links generated";
                if (postingGenStatus == -1)
                {
                    StatusLabel.Text = "Wrong file format order provided, generation halted";
                }
            }
            SeedboxComboBox.IsEnabled = true;
        }

        private string ParseFilename(string filename, string file, string generatedLink)
        {
            string fileInfo = filename;
            string htmltext = "";
            int value;
            double filesize = 0;
            string sizeformat = "B";
            string[] fileInfoSubStrings;

            Console.WriteLine("Starting file parse");

            htmltext = "<p style=\"text-align: center;\">";
            if (postingGenStatus == 0)
            {
                //deletes all bracket enclosed values in the string
                fileInfo = Regex.Replace(fileInfo, @"\[([^\]]*)\]", "");

                //removes file format values from the string (such as .mkv.torrent.zip.anything)
                fileInfo = Regex.Replace(fileInfo, @"[.][A-Za-z0-9\s.]+", "");

                fileInfo = fileInfo.Trim();

                //splits string by whitespace and read backwards to find first substring that is an episode number or identifier such as OP1 or ED
                fileInfoSubStrings = Regex.Split(fileInfo, " ");
                fileInfo = fileInfoSubStrings.Last();
                Console.WriteLine("before episode num check");

                for (int i = fileInfoSubStrings.Length - 1; i >= 0; i--)
                {
                    fileInfoSubStrings[i] = fileInfoSubStrings[i].Trim();

                    //checks if value is is an int (episode number) else checks if the string is OP/OPX/ED/EDX
                    if (int.TryParse(fileInfoSubStrings[i], out value))
                    {
                        fileInfo = fileInfoSubStrings[i];
                        i = -1;
                    }
                    else if (fileInfoSubStrings[i].Length <= 3 && (fileInfoSubStrings[i][0] == 'O' || fileInfoSubStrings[i][0] == 'E') && (fileInfoSubStrings[i][1] == 'P' || fileInfoSubStrings[i][1] == 'D'))
                    {
                        if (fileInfoSubStrings[i].Length == 3)
                        {
                            if (Char.IsDigit(fileInfoSubStrings[i][2]))
                            {
                                fileInfo = fileInfoSubStrings[i];
                                i = -1;
                            }
                        }
                        else
                        {
                            fileInfo = fileInfoSubStrings[i];
                            i = -1;
                        }
                    }
                    else if (fileInfoSubStrings[i].Length <= 5 && (fileInfoSubStrings[i][0] == 'N' && fileInfoSubStrings[i][1] == 'C') && (fileInfoSubStrings[i][2] == 'O' || fileInfoSubStrings[i][2] == 'E') && (fileInfoSubStrings[i][3] == 'P' || fileInfoSubStrings[i][3] == 'D'))
                    {
                        if (fileInfoSubStrings[i].Length == 5)
                        {
                            if (Char.IsDigit(fileInfoSubStrings[i][4]))
                            {
                                fileInfo = fileInfoSubStrings[i];
                                i = -1;
                            }
                        }
                        else
                        {
                            fileInfo = fileInfoSubStrings[i];
                            i = -1;
                        }
                    }
                    else
                    {
                        fileInfo = "XX";
                        i = -1;
                    }
                }
                Console.WriteLine("After episode num check");

                if (int.TryParse(fileInfo, out value))
                {
                    htmltext += "Episode – ";
                }
            }
            else
            {
                htmltext += "‏‏‏‏‏</p>\n";
                htmltext += "<p style=\"text-align: center;\">";
                fileInfo = "Batch";
            }
            htmltext += "<strong>" + fileInfo + "</strong> – (";

            //Console.WriteLine("htmltext is: " + htmltext);

            filesize = new FileInfo(file).Length;
            //Console.WriteLine("filesize in bytes is: " + filesize);

            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            if (postingGenStatus == 0)
            {
                totalFilesize += filesize;
                while (filesize >= 1024 && ++order < sizes.Length)
                {
                    filesize = filesize / 1024;
                    sizeformat = sizes[order];
                }

                //Console.WriteLine("filesize nonformatted: " + filesize.ToString() + sizeformat);

                if (filesize < 100)
                {
                    htmltext += filesize.ToString("0##");
                    //Console.WriteLine("filesize formatted: " + filesize.ToString("0##") + sizeformat);
                }
                else
                {
                    htmltext += filesize.ToString("F0");
                    //Console.WriteLine("filesize formatted: " + filesize.ToString("F0") + sizeformat);
                }
                htmltext += " <em>" + sizeformat + "</em>): <strong><a href=\"" + generatedLink + "\" target=\"_blank\">DDL</a></strong>";
            }
            else if (postingGenStatus == 1)
            {
                while (totalFilesize >= 1024 && ++order < sizes.Length)
                {
                    totalFilesize = totalFilesize / 1024;
                    sizeformat = sizes[order];
                }
                htmltext += totalFilesize.ToString("F2");
                htmltext += " <em>" + sizeformat + "</em>): <strong><a href=\"" + generatedLink + "\" target=\"_blank\">Torrent</a></strong></p>";
            }
            
            return htmltext;
        }

        private void ProcessFile(string file, string directory = "")
        {
            Console.WriteLine("processing file");
            string filename = System.IO.Path.GetFileName(file);
            string format = System.IO.Path.GetExtension(filename);
            string htmlFilename = "";

            if (directory != "")
            {
                directory = directory + '/';
            }
            ///Console.WriteLine("final directory: " + directory);
            HttpWebRequest request = null;
            ///generator.ReportProgress(0, directory + '\n' + filename + '\n');
            
            if (optionsValue == 0)
            {
                generator.ReportProgress(0, filename + '\n');
            }

            ///Console.WriteLine(directory + filename);
            //Console.WriteLine("filename is " + filename);
            directory = directory.Replace(" ", "%20");
            htmlFilename = filename.Replace(" ", "%20");


            if (format.Equals(".mkv") || format.Equals(".zip"))
            {
                if (!(String.IsNullOrEmpty(overwriteText)))
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/" + overwriteText + "/" + directory + htmlFilename);
                }
                else if (seedboxValue == 0)
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://data.project-gxs.com/" + directory + htmlFilename);
                }
                else if (seedboxValue == 1)
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://ddl2.project-gxs.com/" + directory + htmlFilename);
                }
                //else if (seedboxValue == 2)
                //{
                //    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://ddl.project-gxs.com/" + directory + htmlFilename);
                //}
                //else if (seedboxValue == 3)
                //{
                //    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://ddl2.project-gxs.com/" + directory + htmlFilename);
                //}
                else
                {
                    if (optionsValue == 0)
                    {
                        generator.ReportProgress(0, "Invalid file format" + "\n\n");
                    }
                }
            }
            else if (format.Equals(".torrent"))
            {
                if (!(String.IsNullOrEmpty(overwriteText)))
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/" + overwriteText + "/" + directory + htmlFilename);
                }
                else if (seedboxValue == 0)
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://torrents.project-gxs.com/" + htmlFilename);
                }
                else if (seedboxValue == 1)
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://tor2.project-gxs.com/" + htmlFilename);
                }
                //else if (seedboxValue == 2)
                //{
                //    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://og.project-gxs.com/" + htmlFilename);
                //}
                //else if (seedboxValue == 3)
                //{
                //    request = (HttpWebRequest)WebRequest.Create("https://api.shorte.st/s/9960e42c58372da26445d69d348e0baf/http://tv.project-gxs.com/" + htmlFilename);
                //}
                else
                {
                    if (optionsValue == 0)
                    {
                        generator.ReportProgress(0, "Invalid file format" + "\n\n");
                    }
                }
            }
            else
            {
                if (optionsValue == 0)
                {
                    generator.ReportProgress(0, "Invalid file format" + "\n\n");
                }
            }
            Console.WriteLine("Link shortened");

            if (format.Equals(".mkv") || format.Equals(".zip") || format.Equals(".torrent"))
            {
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader sr = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                    string result = sr.ReadToEnd();
                    ///Console.WriteLine(result);
                    string[] words = result.Split(',','"');
                    string generatedLink = "";
                    string htmltext = "";

                    if (!words[3].Equals("ok"))
                    {
                        generator.ReportProgress(0, "Error generating link\n\n");
                    }
                    else
                    {
                        generatedLink = words[8].Replace(@"\", "");
                        Console.WriteLine("Link generated");
                        if (optionsValue == 0)
                        {
                            generatedLink += "\n\n";
                            generator.ReportProgress(0, generatedLink);
                        }
                        else
                        {
                            if (optionsValue == 1)
                            {
                                if (postingGenStatus == 0)
                                {
                                    if (!format.Equals(".torrent"))
                                    {
                                        htmltext = ParseFilename(filename, file, generatedLink);
                                        postingGenStatus = 1;
                                    }
                                    else
                                    {
                                        postingGenStatus = -1;
                                        generator.CancelAsync();
                                    }
                                }
                                else if (postingGenStatus == 1)
                                {
                                    if (format.Equals(".torrent"))
                                    {
                                        htmltext += " | <strong><a href=\"" + generatedLink + "\" target=\"_blank\">Torrent</a></strong></p>\n";
                                        postingGenStatus = 0;
                                    }
                                    else
                                    {
                                        postingGenStatus = -1;
                                        generator.CancelAsync();
                                    }
                                }
                                
                            }
                            else if (optionsValue == 2)
                            {
                                if (!format.Equals(".torrent"))
                                {
                                    htmltext = ParseFilename(filename, file, generatedLink);
                                    htmltext += "</p>";
                                }
                                else
                                {
                                    //postingGenStatus = 1;
                                    batchTorrentFile = file;
                                    batchTorrentLink = generatedLink;
                                    //batchTorrentHtml = ParseFilename(filename, file, generatedLink) + '\n';
                                }
                            }
                            //Console.WriteLine("Filename is: " + filename);


                            if (optionsValue != 1)
                                htmltext += '\n';
                            

                            Console.WriteLine("print htmltext");
                            //Console.Write("html filename is" + htmltext);

                            if (optionsValue != 2)
                                generator.ReportProgress(0, htmltext);
                            else if (!format.Equals(".torrent"))
                                generator.ReportProgress(0, htmltext);
                            
                        }
                    }
                    
                }
                catch (WebException e)
                {
                    generator.ReportProgress(0, "Invalid Request Submitted");
                    ///Console.WriteLine("Invalid Request Submitted");
                }
            }
        }


        private void ProcessDirectory(string path, string prevDirectory = "")
        {
            ///Console.WriteLine("prev dir: " + prevDirectory);
            string directory = System.IO.Path.GetFileName(path);
            string[] fileEntries = Directory.GetFiles(path);
            Array.Sort(fileEntries);

            if (prevDirectory != "")
            {
                directory = prevDirectory + "/" + directory;
            }
            ///Console.WriteLine("directory: " + directory + '\n');
            foreach (string file in fileEntries)
            {
                ///Console.WriteLine("process: " + file);
                ProcessFile(file, directory);
            }

            ///Console.WriteLine("doing subdirectories");
            string[] subdirectories = Directory.GetDirectories(path);
            foreach(string subdirectory in subdirectories)
            {
                string curDir = "";
                if (prevDirectory == "")
                    curDir = directory;
                else
                    curDir = prevDirectory + "/" + directory;
                ///Console.WriteLine("pass subdirectory: " + subdirectory);
                ///Console.WriteLine("pass curdir: " + curDir);
                ProcessDirectory(subdirectory, curDir);
            }
        }
    }
}
