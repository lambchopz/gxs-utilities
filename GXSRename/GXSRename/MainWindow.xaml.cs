﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.ComponentModel;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace GXSRename
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary> 
    class FileInfo
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public int Level { get; set; }
        public Boolean IsDirectory { get; set; }
    }
    /// <summary>
    /// GUI Elements cannot be accessed or modified in the background thread. 
    /// Information must be passed via reportprogress to the main thread.
    /// Values must be saved in the main thread as variables before background thread is launched.
    /// </summary>
    public partial class MainWindow : Window
    {
        private ArrayList fileList = new ArrayList();
        private ArrayList renamedList = new ArrayList();
        private BackgroundWorker fileGenerator = new BackgroundWorker();
        private string groupName = "";
        private string videoInfo = "";
        private string audioInfo = "";
        private int CRCSelection = 1;
        private int state = 0;          //This value is the current state of the GUI. -1=process canceled. 0=waiting on files. 1=Files dropped/generating filenames. 2=Rename done,can undo changes or drag new files


        public MainWindow()
        {
            InitializeComponent();
            fileGenerator.WorkerReportsProgress = true;
            fileGenerator.WorkerSupportsCancellation = true;
            fileGenerator.DoWork += new DoWorkEventHandler(Generator_DoWork);
            fileGenerator.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Generator_RunWorkerCompleted);
            fileGenerator.ProgressChanged += Generator_ProgressChanged;
        }

        public void OnDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }

        private void OnDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            try
            {
                foreach (string file in files)
                {
                    break;
                }
            }
            catch
            {
                StatusLabel.Text = "Some directories or files provided failed to load. Check if the file is opened or still exists.";
                return;
            }
            state = 1;
            StatusLabel.Text = "Generating filenames...";
            StartStopButton.Content = "Stop Generation";
            DisableElements();
            groupName = GroupTextBox.Text;
            videoInfo = VideoTextBox.Text;
            audioInfo = AudioTextBox.Text;
            CRCSelection = CRCComboBox.SelectedIndex;

            FileListTextBox.Text = "";
            RenamedListTextBox.Text = "";
            fileList.Clear();
            renamedList.Clear();
            if (!fileGenerator.IsBusy)
            {
                fileGenerator.RunWorkerAsync(files);
            }
            else
            {
                fileGenerator.CancelAsync();
                fileGenerator.RunWorkerAsync(files);
            }
        }

        private void DisableElements()
        {
            GroupTextBox.IsEnabled = false;
            VideoTextBox.IsEnabled = false;
            AudioTextBox.IsEnabled = false;
            CRCComboBox.IsEnabled = false;
            StartStopButton.IsEnabled = true;
            FileListTextBox.AllowDrop = false;
            RenamedListTextBox.AllowDrop = false;
        }

        private void EnableElements()
        {
            GroupTextBox.IsEnabled = true;
            VideoTextBox.IsEnabled = true;
            AudioTextBox.IsEnabled = true;
            CRCComboBox.IsEnabled = true;
            StartStopButton.IsEnabled = true;
            FileListTextBox.AllowDrop = true;
            RenamedListTextBox.AllowDrop = true;
        }

        private void Generator_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] files = (string[])e.Argument;

            foreach (string file in files)
            {
                if (Directory.Exists(file))
                {
                    ProcessDirectory(file);
                }
                else if (File.Exists(file))
                {
                    ProcessFile(file, 0);
                }
                if (fileGenerator.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
            }
            GenerateRenamePreview(e);
        }

        private void Generator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
            {
                FileListTextBox.AppendText(e.UserState as string);
            }
            else
            {
                RenamedListTextBox.AppendText(e.UserState as string);
            }
        }

        private void Generator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (state == -1)
                {
                    StatusLabel.Text = "Process canceled";
                    state = 0;
                    EnableElements();
                    StartStopButton.IsEnabled = false;
                    StartStopButton.Content = "Start Rename";
                }
                else
                {
                    StatusLabel.Text = "Error, process stopped";
                    state = 0;
                    EnableElements();
                    StartStopButton.IsEnabled = false;
                    StartStopButton.Content = "Start Rename";
                }
                FileListTextBox.Text = "";
                RenamedListTextBox.Text = "";
                fileList.Clear();
                renamedList.Clear();
            }
            else
            {
                if (state == 1)
                {
                    state = 2;
                    EnableElements();
                    StartStopButton.Content = "Start Rename";
                    StatusLabel.Text = "Filenames Generated. Start Rename or provide new files";
                }
            }
            //SeedboxComboBox.IsEnabled = true;
        }

        private void ProcessFile(string file, int level)
        {
            string filename = System.IO.Path.GetFileName(file);
            string formattedFilename = filename;
            string format = System.IO.Path.GetExtension(filename);

            if (level != 0)
            {
                formattedFilename = "|  " + formattedFilename;
            }
            for (int indent = 0; indent < level; indent++)
            {
                formattedFilename = "|  " + formattedFilename;
            }

            //fileGenerator.ReportProgress(0, directory + filename + '\n');
            //Console.WriteLine(directory + filename);
            //directory = directory.Replace(" ", "%20");
            //filename.Replace(" ", "%20");

            if (format.Equals(".mkv") || format.Equals(".gxs") || format.Equals(".torrent"))
            {
                fileList.Add(new FileInfo() { FilePath = file, FileName = filename, Level = level, IsDirectory = false });
                fileGenerator.ReportProgress(0, formattedFilename + '\n');
            }
        }

        private void ProcessDirectory(string path, int level = 0)
        {
            string directory = System.IO.Path.GetFileName(path);
            fileList.Add(new FileInfo() { FilePath = path, FileName = directory, Level = level, IsDirectory = true });

            string formattedDirectory = "+--" + directory;
            ///Console.WriteLine(directory + '\n');

            for (int indent = 0; indent < level; indent++)
            {
                formattedDirectory = "|  " + formattedDirectory;
            }
            fileGenerator.ReportProgress(0, formattedDirectory + '\n');

            string[] fileEntries = Directory.GetFiles(path);
            foreach (string file in fileEntries)
            {
                ProcessFile(file, level + 1);
            }

            string[] subdirectories = Directory.GetDirectories(path);

            foreach (string subdirectory in subdirectories)
            {
                ProcessDirectory(subdirectory, level + 1);
            }
        }

        private void GenerateRenamePreview(DoWorkEventArgs e)
        {
            string filename = "";
            string format = "";
            string hex = "1234567890abcdefABCDEF";

            foreach (FileInfo file in fileList)
            {
                filename = file.FileName;
                format = System.IO.Path.GetExtension(filename);
                if (format.Equals(".gxs"))
                    format = ".mkv";

                //remove everything surrounded by brackets or perentheses
                //then remove all "invalid" characters in filenames
                filename = filename.Substring(0, filename.Length - format.Length);
                filename = Regex.Replace(filename, @"\(([^)]*)\)|\[([^\]]*)\]", "");
                filename = filename.Replace('_', ' ');
                filename = filename.Replace('\\', ' ');
                filename = filename.Replace('/', ' ');
                filename = filename.Replace(':', ' ');
                filename = filename.Replace(';', ' ');
                filename = filename.Replace('<', ' ');
                filename = filename.Replace('>', ' ');
                filename = filename.Replace('|', ' ');
                filename = filename.Replace('@', ' ');
                filename = filename.Replace('#', ' ');
                filename = filename.Replace('$', ' ');
                filename = filename.Replace('%', ' ');
                filename = filename.Replace('^', ' ');
                filename = filename.Replace('&', ' ');
                filename = filename.Replace('*', ' ');
                filename = filename.Trim();

                if (file.IsDirectory && file.Level > 0)
                {
                    //skip subdirectories
                }
                else
                {
                    if (!groupName.Equals(""))
                        filename = groupName + " " + filename;
                    if (!videoInfo.Equals(""))
                        filename = filename + " " + videoInfo;
                    if (!audioInfo.Equals(""))
                    {
                        if (file.IsDirectory || format.Equals(".torrent"))
                            filename = filename + " " + audioInfo;
                    }
                }
                    
                if (!file.IsDirectory && !format.Equals(".torrent"))
                {
                    if (CRCSelection == 1)
                    {
                        Boolean validCrc = true;
                        string myCrc = file.FileName.Substring(file.FileName.Length - 14, 10);

                        if (myCrc[0] == '[' && myCrc[9] == ']')
                        {
                            for (int value = 1; value < 9; value++)
                            {
                                if (!hex.Contains(myCrc[value]))
                                {
                                    validCrc = false;
                                }
                            }
                        }
                        else
                        {
                            validCrc = false;
                        }
                        if (validCrc)
                            filename = filename + " " + myCrc;
                    }
                    else if (CRCSelection == 2)
                    {
                        Crc32 crc32 = new Crc32();
                        String hash = String.Empty;

                        using (FileStream fs = File.Open(file.FilePath, FileMode.Open))
                            foreach (byte b in crc32.ComputeHash(fs)) hash += b.ToString("x2").ToUpper();

                        filename = filename + " [" + hash + "]";
                    }
                }

                filename = filename + format;

                renamedList.Add(Directory.GetParent(file.FilePath) + @"\" + filename);

                if (file.IsDirectory)
                {
                    filename = "+--" + filename;
                }
                else if (file.Level != 0)
                {
                    filename = "|  " + filename;
                }
                for (int i = 0; i < file.Level; i++)
                {
                    filename = "|  " + filename;
                }
                
                fileGenerator.ReportProgress(1, filename + '\n');

                if (fileGenerator.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void StartStopRename(object sender, RoutedEventArgs e)
        {
            Boolean sameFilename = false;
            if (state == 1)
            {
                state = -1;
                fileGenerator.CancelAsync();
            }
            else if (state == 2)
            {
                DisableElements();
                for (int index = 0; index < fileList.Count; ++index)
                {
                    if (!((FileInfo)fileList[index]).IsDirectory)
                    {
                        try
                        {
                            File.Move(((FileInfo)fileList[index]).FilePath, renamedList[index].ToString());
                        }
                        catch
                        {
                            sameFilename = true;
                        }
                    }
                }

                for (int index = 0; index < fileList.Count; ++index)
                {
                    if (((FileInfo)fileList[index]).IsDirectory)
                    {
                        try
                        {
                            Directory.Move(((FileInfo)fileList[index]).FilePath, renamedList[index].ToString());
                        }
                        catch
                        {
                            if (((FileInfo)fileList[index]).Level > 0)
                            {
                                Console.WriteLine("Skip Renaming Subdirectories");
                            }
                            else
                            {
                                sameFilename = true;
                            }
                        }
                    }
                }
                if (sameFilename == true)
                {
                    StatusLabel.Text = "Files renamed. Some files could not be renamed. Check if those files are opened or if the filenames already exist. Waiting for new files";
                }
                else
                {
                    StatusLabel.Text = "All files have been renamed. Waiting for new files";
                }
                MessageBox.Show("I accept your pathetic sacrifice and have granted your files a new name!");
                Console.WriteLine("Done");
                EnableElements();
                StartStopButton.IsEnabled = false;
                state = 0;
            }
        }
    }
}
